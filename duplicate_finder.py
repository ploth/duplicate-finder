from pathlib import Path

def parse_args():
    import argparse

    arg_parser = argparse.ArgumentParser(description="Get duplicates",
                                         formatter_class=argparse.RawTextHelpFormatter)
    arg_parser.add_argument("-i", "--input", type=str, action='append', help="Input folders")
    args = arg_parser.parse_args()
    return args

if __name__ == '__main__':
    args = parse_args()
    folders = [Path(folder) for folder in args.input]
    folder_sets = []
    for folder in folders:
        files = folder.glob('**/*')
        files = [file.name for file in files]
        folder_sets.append(set(files))

    duplicates = folder_sets[0]
    for set in folder_sets:
        duplicates = duplicates & set

    print(f'Duplicates: {len(duplicates)}')
    print(duplicates)
